package com.maybank.JavaWebApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.service.ProviderService;

@SpringBootApplication
public class JavaWebAppApplication implements ApplicationRunner{
	
	@Autowired
	private ProviderService providerService;

	public static void main(String[] args) {
		SpringApplication.run(JavaWebAppApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Start App");
		Provider providerDummy1 = new Provider();
		Provider providerDummy2 = new Provider();
		providerDummy1.setName("Maybank");
		providerDummy2.setName("BCA");
		this.providerService.save(providerDummy1);
		this.providerService.save(providerDummy2);
	}

}
