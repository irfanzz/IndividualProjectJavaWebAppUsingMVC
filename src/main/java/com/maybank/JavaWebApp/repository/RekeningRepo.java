package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.Rekening;

public interface RekeningRepo extends JpaRepository<Rekening, Long>{

	Rekening findByNoRekening(String noRekening);

}
