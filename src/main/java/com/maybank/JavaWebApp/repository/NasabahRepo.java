package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.Nasabah;

public interface NasabahRepo extends JpaRepository<Nasabah, Long>{

}
