package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.HistoryTransfer;

public interface HistoryTransferRepo extends JpaRepository<HistoryTransfer, Long>{

}
