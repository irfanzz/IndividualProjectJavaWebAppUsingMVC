package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.Role;

public interface RoleRepo extends JpaRepository<Role, Long>{

}
