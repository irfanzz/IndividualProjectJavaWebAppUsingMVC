package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.Provider;

public interface ProviderRepo extends JpaRepository<Provider, Long>{

}
