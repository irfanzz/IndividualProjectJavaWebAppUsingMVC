package com.maybank.JavaWebApp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.entity.TransferAmount;

public interface TransferAmountRepo extends JpaRepository<TransferAmount, Long>{
//	Optional<Rekening> getRekeningByNoRek(String noRekening);
}
