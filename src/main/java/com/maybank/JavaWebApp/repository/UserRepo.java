package com.maybank.JavaWebApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.JavaWebApp.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{
	User findByUsername(String username);
}
