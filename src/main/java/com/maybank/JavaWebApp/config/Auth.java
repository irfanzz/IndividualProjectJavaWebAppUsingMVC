package com.maybank.JavaWebApp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.JavaWebApp.handler.UserAuthHandler;
import com.maybank.JavaWebApp.service.CustomerUserDetailsService;

@Configuration
@EnableWebSecurity
public class Auth {
	
	@Autowired
	private UserAuthHandler authHandler;
	
	@Autowired
	private CustomerUserDetailsService customerUserDetailsService;
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customerUserDetailsService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}
	@Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity)throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/provider")
        .hasAnyAuthority("admin");
        httpSecurity.authorizeRequests().antMatchers("/nasabah")
        .hasAnyAuthority("customerService");
        httpSecurity.authorizeRequests().antMatchers("/transfer")
        .hasAnyAuthority("operator");
//        httpSecurity.authorizeRequests().anyRequest().authenticated();
        httpSecurity.authorizeRequests().and().formLogin().successHandler(authHandler);

        return httpSecurity.build();
    }

}
