package com.maybank.JavaWebApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.service.NasabahService;

@RestController
@RequestMapping("/nasabah")
public class NasApiController {
	
	@Autowired
	private NasabahService nasabahService;
	
	@GetMapping("/api")
	public List<Nasabah> getAll() {
		return nasabahService.getAll();
	}

}
