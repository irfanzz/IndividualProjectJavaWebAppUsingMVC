package com.maybank.JavaWebApp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.JavaWebApp.entity.HistoryTransfer;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.entity.TransferAmount;
import com.maybank.JavaWebApp.service.TransferAmountService;

@Controller
@RequestMapping("/transfer")
public class TransferController {
	
	@Autowired
	private TransferAmountService service;
	
	@GetMapping
	public String index(@RequestParam(value = "fee", defaultValue = "0") Double fee,
			Model model) {
		List<TransferAmount> listAmounts = service.getAll();
		model.addAttribute("tf", new TransferAmount());
		model.addAttribute("list", listAmounts);
		return "transfer";
	}
	
	@PostMapping("/kirim")
	public String kirim(@RequestParam(value = "fee", defaultValue = "0") Double fee,
			@Valid @ModelAttribute("tf") TransferAmount transferAmount, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		List<TransferAmount> listTf = service.getAll();
		if(result.hasErrors()) {
			model.addAttribute("list", listTf);
			return "transfer";
		}else {
			Rekening rekening1=service.findByNoRekening(transferAmount.getPengirim().getNoRekening());
			Rekening rekening2=service.findByNoRekening(transferAmount.getPenerima().getNoRekening());
			if(rekening1 == null || rekening2 == null) {
				model.addAttribute("list", listTf);
				return "redirect:/transfer";
			}else {
				if(rekening1.getProvider()!=rekening2.getProvider()) {
					fee = 6500.0;
				}
				rekening1.setSaldo(rekening1.getSaldo()-transferAmount.getJumlahAmount()-fee);
				if(rekening1.getSaldo()<50000.0) {
					redirectAttributes.addFlashAttribute("gagal", "Saldo Pengirim kurang dari Rp50000, Transaksi Gagal");
					rekening1.setSaldo(rekening1.getSaldo()+transferAmount.getJumlahAmount()+fee);
				}else {
					rekening2.setSaldo(rekening2.getSaldo()+transferAmount.getJumlahAmount());
					transferAmount.setPengirim(rekening1);
					transferAmount.setPenerima(rekening2);
					transferAmount.setFee(fee);
					HistoryTransfer historyTransfer = new HistoryTransfer();
					this.service.save(transferAmount,historyTransfer);
				}
			}
		}
		return "redirect:/transfer";
	}
	@PostMapping("/cek/{noRekening}")
	public @ResponseBody List<Rekening> findRekByNoRek(@PathVariable("noRekening")String noRek,
			Model model){
		List<TransferAmount> list = this.service.getAll();
		Rekening rekening = service.findByNoRekening(noRek);
		if(rekening.equals(null)) {
			return null;
		}
		return (List<Rekening>) rekening;
	}

}
