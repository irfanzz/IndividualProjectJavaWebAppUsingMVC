package com.maybank.JavaWebApp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.repository.ProviderRepo;
import com.maybank.JavaWebApp.service.ProviderService;

@Controller
@RequestMapping("/provider")
public class ProviderController {
	
	@Autowired
	private ProviderService providerService;
	
	@GetMapping
	public String index(Model model) {
		List<Provider> providers = providerService.getAll();
		model.addAttribute("provider", new Provider());
		model.addAttribute("providers", providers);
		return "provider";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("provider") Provider provider,
			BindingResult result, Model model) {
		List<Provider> providers = providerService.getAll();
		if(result.hasErrors()) {
			model.addAttribute("providers", providers);
			return "provider";
		}
		this.providerService.save(provider);
		System.out.println(provider.getName());
		return "redirect:/provider";
	}

}
