package com.maybank.JavaWebApp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.service.NasabahService;
import com.maybank.JavaWebApp.service.ProviderService;
import com.maybank.JavaWebApp.service.RekeningService;

@Controller
@RequestMapping("/nasabah")
public class NasabahController {
	
	@Autowired
	private NasabahService nasabahService;
	
	@GetMapping
	public String index(Model model) {
		List<Nasabah> nasabahs = nasabahService.getAll();
		List<Provider> providers = nasabahService.getProvider();
		model.addAttribute("nasabahForm", new Nasabah());
		model.addAttribute("rek", new Rekening());
		model.addAttribute("provider", providers);
		model.addAttribute("nasabahs", nasabahs);
		return "nasabah";
	}
	
	@PostMapping("/tambah")
	public String save(@Valid @ModelAttribute("nasabahForm") Nasabah nasabah,
			BindingResult result,
			@Valid @ModelAttribute("rek") Rekening rekening,
			BindingResult result2,
			@Valid @ModelAttribute("provider") Provider provider,
			Model model) {
		List<Nasabah> nasabahs = nasabahService.getAll();
		if(result.hasErrors()||result2.hasErrors()) {
			model.addAttribute("nasabahs", nasabahs);
			return "nasabah";
		}
		System.out.println("Nama Lengkap Nasabah : "+nasabah.getNamaLengkap());
		System.out.println("Tanggal Lahir Nasabah : "+nasabah.getTanggalLahir());
		System.out.println("No Identitas Nasabah: "+nasabah.getNoIdentitas());
		System.out.println("Tipe Identitas Nasabah: "+nasabah.getTipeIdentitas());
		System.out.println("No Contact Nasabah: "+nasabah.getNoContact());
		System.out.println("Email Nasabah: "+nasabah.getEmail());
		System.out.println("No Rekening: "+rekening.getNoRekening());
		System.out.println("Bank Provider: "+provider.getName());
		this.nasabahService.save(nasabah, rekening, provider);
		return "redirect:/nasabah";
	}

}
