package com.maybank.JavaWebApp.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Rekening")
public class Rekening {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@NotEmpty
	@NotBlank
	@Column(name = "noRekening", unique = true)
	@Size(max = 16)
	private String noRekening;
	private Double saldo = 10000000.0;
	@ManyToOne
	@JsonIgnore
	private Nasabah nasabah;
	@ManyToOne
	@JsonIgnore
	private Provider provider;
	@OneToMany(mappedBy = "pengirim", fetch = FetchType.EAGER)
	private List<TransferAmount> pengirim;
	@OneToMany(mappedBy = "penerima", fetch = FetchType.EAGER)
	private List<TransferAmount> penerima;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNoRekening() {
		return noRekening;
	}
	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Nasabah getNasabah() {
		return nasabah;
	}
	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	public List<TransferAmount> getPengirim() {
		return pengirim;
	}
	public void setPengirim(List<TransferAmount> pengirim) {
		this.pengirim = pengirim;
	}
	public List<TransferAmount> getPenerima() {
		return penerima;
	}
	public void setPenerima(List<TransferAmount> penerima) {
		this.penerima = penerima;
	}

	
}
