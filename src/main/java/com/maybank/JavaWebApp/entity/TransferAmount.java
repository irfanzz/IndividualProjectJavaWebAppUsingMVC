package com.maybank.JavaWebApp.entity;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "transferAmount")
public class TransferAmount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@NotNull
	private Rekening pengirim;
	@ManyToOne
	@NotNull
	private Rekening penerima;
	@NotNull
	private Date tanggalKirim = Date.valueOf(LocalDate.now());
	private Double jumlahAmount;
	private Double fee;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Rekening getPengirim() {
		return pengirim;
	}
	public void setPengirim(Rekening pengirim) {
		this.pengirim = pengirim;
	}
	public Rekening getPenerima() {
		return penerima;
	}
	public void setPenerima(Rekening penerima) {
		this.penerima = penerima;
	}
	public Date getTanggalKirim() {
		return tanggalKirim;
	}
	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}
	public Double getJumlahAmount() {
		return jumlahAmount;
	}
	public void setJumlahAmount(Double jumlahAmount) {
		this.jumlahAmount = jumlahAmount;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}

}
