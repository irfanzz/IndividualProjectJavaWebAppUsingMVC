package com.maybank.JavaWebApp.entity;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Nasabah")
public class Nasabah {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@NotBlank
	@NotEmpty
	@Column(name = "NamaLengkap")
	private String namaLengkap;
	@NotNull(message = "date must not be blank")
	private Date tanggalLahir;
	@Size(max = 16)
	private String noIdentitas;
	private String tipeIdentitas;
	private String email;
	@Size(max = 12)
	private String noContact;
	@OneToMany(mappedBy = "nasabah",fetch = FetchType.EAGER)
	private List<Rekening> rekenings;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public List<Rekening> getRekenings() {
		return rekenings;
	}
	public void setRekenings(List<Rekening> rekenings) {
		this.rekenings = rekenings;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getTipeIdentitas() {
		return tipeIdentitas;
	}
	public void setTipeIdentitas(String tipeIdentitas) {
		this.tipeIdentitas = tipeIdentitas;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoContact() {
		return noContact;
	}
	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}

}
