package com.maybank.JavaWebApp.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "historyTransfer")
public class HistoryTransfer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String rekeningPengirim;
	private String rekeningPenerima;
	private Date tanggalKirim;
	private String createUser;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRekeningPengirim() {
		return rekeningPengirim;
	}
	public void setRekeningPengirim(String rekeningPengirim) {
		this.rekeningPengirim = rekeningPengirim;
	}
	public String getRekeningPenerima() {
		return rekeningPenerima;
	}
	public void setRekeningPenerima(String rekeningPenerima) {
		this.rekeningPenerima = rekeningPenerima;
	}
	public Date getTanggalKirim() {
		return tanggalKirim;
	}
	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	

}
