package com.maybank.JavaWebApp.service;

import java.util.List;
import java.util.Optional;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.entity.Rekening;

public interface NasabahService {
	
	public List<Nasabah> getAll();
	public void save(Nasabah nasabah, Rekening rekening, Provider provider);
	public Optional<Nasabah> getNasabahById(Long id);
	public List<Provider> getProvider();

}
