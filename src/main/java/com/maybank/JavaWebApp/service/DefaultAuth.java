package com.maybank.JavaWebApp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.repository.RoleRepo;
import com.maybank.JavaWebApp.repository.UserRepo;
import com.maybank.JavaWebApp.entity.Role;
import com.maybank.JavaWebApp.entity.User;

@Service
@Transactional
public class DefaultAuth {
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	@PostConstruct
	public void index() {
//		Create Role
		Role roleAdmin = new Role();
		Role roleCustomerService = new Role();
		Role roleOperator = new Role();
		
		roleAdmin.setRole("admin");
		roleCustomerService.setRole("customerService");
		roleOperator.setRole("operator");
		this.roleRepo.save(roleAdmin);
		this.roleRepo.save(roleCustomerService);
		this.roleRepo.save(roleOperator);
		
//		create user
		List<Role> adminRole = new ArrayList<>();
		List<Role> csRole = new ArrayList<>();
		List<Role> operatorRole = new ArrayList<>();
		adminRole.add(roleAdmin);
		csRole.add(roleCustomerService);
		operatorRole.add(roleOperator);
		
		User userAdmin = new User();
		userAdmin.setUsername("maybank");
		userAdmin.setEmail("admin@maybank.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userAdmin.setRoles(adminRole);
		
		User userCs = new User();
		userCs.setUsername("irfan");
		userCs.setEmail("irfan@maybank.cs");
		userCs.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userCs.setRoles(csRole);
		
		User userOperator = new User();
		userOperator.setUsername("operator");
		userOperator.setEmail("tamara@maybank.operator");
		userOperator.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userOperator.setRoles(operatorRole);
		
		this.userRepo.save(userAdmin);
		this.userRepo.save(userCs);
		this.userRepo.save(userOperator);
	}

}
