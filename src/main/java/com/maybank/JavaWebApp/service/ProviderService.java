package com.maybank.JavaWebApp.service;

import java.util.List;

import com.maybank.JavaWebApp.entity.Provider;

public interface ProviderService {
	
	public List<Provider> getAll();
	public void save(Provider provider);

}
