package com.maybank.JavaWebApp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.repository.NasabahRepo;
import com.maybank.JavaWebApp.repository.ProviderRepo;
import com.maybank.JavaWebApp.repository.RekeningRepo;

@Service
@Transactional
public class NasabahServiceImpl implements NasabahService{
	
	@Autowired
	private NasabahRepo nasabahRepo;
	@Autowired
	private RekeningRepo rekeningRepo;
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findAll();
	}

	@Override
	public void save(Nasabah nasabah, Rekening rekening, Provider provider) {
		// TODO Auto-generated method stub
		rekening.setNasabah(nasabah);
		rekening.setProvider(provider);
		this.nasabahRepo.save(nasabah);
		this.rekeningRepo.save(rekening);
	}

	@Override
	public Optional<Nasabah> getNasabahById(Long id) {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findById(id);
	}

	@Override
	public List<Provider> getProvider() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

}
