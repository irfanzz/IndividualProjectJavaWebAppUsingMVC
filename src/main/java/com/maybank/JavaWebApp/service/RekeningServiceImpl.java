package com.maybank.JavaWebApp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.repository.RekeningRepo;

@Service
@Transactional
public class RekeningServiceImpl implements RekeningService{
	
	@Autowired
	private RekeningRepo rekeningRepo;

	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findAll();
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
		this.rekeningRepo.save(rekening);
	}

	@Override
	public void getNasabah(Nasabah nasabah) {
		// TODO Auto-generated method stub
		nasabah.getNamaLengkap();
	}

	@Override
	public void getProvider(Provider provider) {
		// TODO Auto-generated method stub
		provider.getName();
	}

	@Override
	public Rekening findByNoRekening(String noRekening) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findByNoRekening(noRekening);
	}

}
