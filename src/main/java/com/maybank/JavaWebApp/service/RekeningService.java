package com.maybank.JavaWebApp.service;

import java.util.List;

import com.maybank.JavaWebApp.entity.Nasabah;
import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.entity.Rekening;

public interface RekeningService {
	
	public List<Rekening> getAll();
	public void save(Rekening rekening);
	public void getNasabah(Nasabah nasabah);
	public void getProvider(Provider provider);
	public Rekening findByNoRekening(String noRekening);

}
