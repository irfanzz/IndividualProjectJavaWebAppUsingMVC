package com.maybank.JavaWebApp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.entity.HistoryTransfer;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.entity.TransferAmount;
import com.maybank.JavaWebApp.repository.HistoryTransferRepo;
import com.maybank.JavaWebApp.repository.RekeningRepo;
import com.maybank.JavaWebApp.repository.TransferAmountRepo;

@Service
@Transactional
public class TransferAmountServiceImpl implements TransferAmountService{
	
	@Autowired
	private TransferAmountRepo repo;
	@Autowired
	private RekeningRepo rekeningRepo;
	@Autowired
	private HistoryTransferRepo historyTransferRepo;

	@Override
	public List<TransferAmount> getAll() {
		// TODO Auto-generated method stub
		return this.repo.findAll();
	}

	@Override
	public void save(TransferAmount transferAmount, HistoryTransfer historyTransfer) {
		// TODO Auto-generated method stub
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		historyTransfer.setRekeningPenerima(transferAmount.getPenerima().getNoRekening());
		historyTransfer.setRekeningPengirim(transferAmount.getPengirim().getNoRekening());
		historyTransfer.setTanggalKirim(transferAmount.getTanggalKirim());
		historyTransfer.setCreateUser(currentPrincipalName);
		this.repo.save(transferAmount);
		this.historyTransferRepo.save(historyTransfer);
	}

	@Override
	public Rekening findByNoRekening(String noRekening) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findByNoRekening(noRekening);
	}

}
