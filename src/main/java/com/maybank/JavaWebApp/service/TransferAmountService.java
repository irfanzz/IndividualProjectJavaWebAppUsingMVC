package com.maybank.JavaWebApp.service;

import java.util.List;

import com.maybank.JavaWebApp.entity.HistoryTransfer;
import com.maybank.JavaWebApp.entity.Rekening;
import com.maybank.JavaWebApp.entity.TransferAmount;

public interface TransferAmountService {
	
	public List<TransferAmount> getAll();
	public void save(TransferAmount transferAmount, HistoryTransfer historyTransfer);
	public Rekening findByNoRekening(String noRekening);

}
