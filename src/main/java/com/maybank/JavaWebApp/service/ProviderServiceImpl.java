package com.maybank.JavaWebApp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.entity.Provider;
import com.maybank.JavaWebApp.repository.ProviderRepo;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService{
	
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.save(provider);
	}

}
