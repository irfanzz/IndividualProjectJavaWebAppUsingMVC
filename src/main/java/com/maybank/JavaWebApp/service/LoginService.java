package com.maybank.JavaWebApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.repository.RoleRepo;
import com.maybank.JavaWebApp.repository.UserRepo;
import com.maybank.JavaWebApp.entity.User;

@Service
public class LoginService {
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	public User findUserByUsername(String username) {
		return this.userRepo.findByUsername(username);
	}
	
	public void addUser(User user) {
		this.userRepo.save(user);
	}
	
	public List<User> findAll(){
		return this.userRepo.findAll();
	}

}
