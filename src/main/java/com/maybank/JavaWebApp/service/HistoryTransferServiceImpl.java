package com.maybank.JavaWebApp.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.JavaWebApp.entity.HistoryTransfer;
import com.maybank.JavaWebApp.repository.HistoryTransferRepo;

@Service
@Transactional
public class HistoryTransferServiceImpl implements HistoryTransferService{
	
	@Autowired
	private HistoryTransferRepo repo;

	@Override
	public void save(HistoryTransfer historyTransfer) {
		// TODO Auto-generated method stub
		this.repo.save(historyTransfer);
	}

}
